# SoftSync - fast and robust deploy tool 

```softsync``` est un script Bash permettant de déployer sur un poste client (nœud de calcul par exemple)
un logiciel (une arborescence de fichier) enregistré dans un serveur central via ```rsync```.
C'est une fine surcouche à ```rsync``` qui consomme très peu de ressource et permet de gérer plusieurs distributions ou architectures facilement.

Un moyen simple d'utiliser la dernière version de [source:/trunk/softsync/softsync@head softsync]
sans récupérer tout le repository est de faire :
```bash
wget https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/softsync/-/raw/master/softsync
chmod u+x ./softsync
```

## Dépendances

Sous Debian, ```softsync``` nécessite les paquetages suivants (```rsync, nice, ionice```) :
```bash
apt-get install rsync coreutils util-linux
```
```nice``` et ```ionice``` sont utilisés pour rendre le transfert de fichier lent
et peu consommateur de ressource afin de le rendre transparent pour l'utilisateur.


## Repository

L'ensemble du code est sous **licence libre**.
Le script en ```bash``` est sous GPL version 2 ou plus récente (http://www.gnu.org/licenses/gpl.html).

Tous les sources sont disponibles sur la forge du campus :
https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/softsync/

Les sources sont gérés via git (GitLab).
Il est très facile de rester synchronisé par rapport à ces sources.

 * la récupération initiale
```bash
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/softsync
```
 * les mises à jour par la suite
```bash
git pull
```

Il est possible d'avoir un accès en écriture à la forge
sur demande motivée à [Gabriel Moreau](mailto:Gabriel.Moreau__AT__legi.grenoble-inp.fr).
Pour des questions de temps d'administration et de sécurité,
la forge n'est pas accessible en écriture sans autorisation.
Pour des questions de décentralisation du web, d'autonomie
et de non allégeance au centralisme ambiant (et nord américain),
nous utilisons notre propre forge...

Vous pouvez proposer un patch par courriel d'un fichier particulier via la commande ```diff```.
A noter que ```svn``` propose par défaut le format unifié (```-u```).
Deux exemples :
```bash
diff -u softsync.org softsync.new > softsync.patch
```
On applique le patch (après l'avoir lu et relu) via la commande
```bash
patch -p0 < softsync.patch
```
